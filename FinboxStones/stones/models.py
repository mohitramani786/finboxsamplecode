from django.db import models
from django.contrib.auth.models import User


# Create your models here.


class Stones(models.Model):
    class Meta:
        db_table = "stones"

    Stone_ID = models.AutoField(primary_key=True)
    Stone_Name = models.CharField(max_length=40)


class Activations(models.Model):
    class Meta:
        db_table = "activations"
    Activation_ID = models.AutoField(primary_key=True)
    User_ID = models.ForeignKey(User, on_delete=models.CASCADE)
    Stone_ID = models.ForeignKey(Stones, on_delete=models.CASCADE)
    Start_Time = models.DateTimeField(auto_now_add=True)
    End_Time = models.DateTimeField()

    def create_obj(user_id,stone_id,st,et):
        print(user_id,stone_id,st,et,"------------")
        Activations.objects.create(User_ID_id=user_id,Stone_ID_id=stone_id,Start_Time=st,End_Time=et)
        return None