from django.contrib import admin
from stones.models import Activations, Stones

admin.site.register(Activations)
admin.site.register(Stones)
