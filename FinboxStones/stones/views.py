from turtle import delay
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import authentication_classes, permission_classes, api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework import status as status_code
from django.conf import settings
from django.http import JsonResponse
import logging
from stones.models import *
from datetime import datetime, timedelta, time, date
from django.db.models import Q
from stones.task import activated



@csrf_exempt
@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def activate_stones(request):
    try:

        """ This function/API for Activating the Stones based on stone id and user 
        """
        ct = datetime.now()
        data = {'Power_Duration':request.data.get('Power_Duration'),
                'Stone_ID':request.data.get('Stone_ID'),
                'User_ID':request.data.get('User_ID'),
                "st" : ct,
                "et" : ct+ timedelta(seconds=request.data.get('Power_Duration'))
                }
        
        queries = Q()
        queries = Q(User_ID_id=data['User_ID']) | Q(Stone_ID_id=data['Stone_ID'])
        queries = queries &  (Q(Start_Time__lte=ct) & Q(End_Time__gte=ct))
        activation_obj = Activations.objects.filter(queries)
        if activation_obj:
            return JsonResponse(
                        {"message" : "Stones is already in activated state or Same users has others activated Stones",'data':{'Start_Time':activation_obj[0].Start_Time,"End_Time":activation_obj[0].End_Time},"error" : "" }, status=status_code.HTTP_200_OK)
        else:
            activated.apply_async((data['User_ID'],data['Stone_ID'],data['st'],data['et']))
            return JsonResponse(
                            {"message" : "Activation Records has been created!!",'data':{'Start_Time':data['st'],"End_Time":data['et']},"Error" : ""}, status=status_code.HTTP_200_OK)
    except Exception as e:
        return JsonResponse(
                        {"message" : "Something Went Wrong!!",'data':{},"error" : str(e)}, status=status_code.HTTP_500_INTERNAL_SERVER_ERROR)
                    

@csrf_exempt
@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def get_user_stones_status(request):

    """ This function/API for Fetch the current status of a user's stone activation.
    """

    try:
        user_id = request.GET.get('user_id')
        activation_obj = Activations.objects.filter(User_ID_id=user_id,Start_Time__lte=datetime.now(),End_Time__gte=datetime.now())
        data = {}
        if activation_obj:
            data = {'Stone_ID':activation_obj[0].Stone_ID.Stone_ID , 'Start_Time':activation_obj[0].Start_Time,"End_Time":activation_obj[0].End_Time}
        return JsonResponse(
                        {"message" : "Activated Stones",'data' : data,"error" : ""}, status=status_code.HTTP_200_OK)
    except Exception as e:
        print("=======",str(e))
        return JsonResponse(
                        {"message" : "Something Went Wrong!!",'data' : {},"error" : str(e)}, status=status_code.HTTP_500_INTERNAL_SERVER_ERROR)